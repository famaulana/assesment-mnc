<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->string('pic');
            $table->string('company');
            $table->string('address');
            $table->string('contact');
            $table->string('email');
            $table->string('website');
            $table->string('po_numb');
            $table->string('location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
