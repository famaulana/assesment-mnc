<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
           'email' => 'email|required',
           'password' => 'required',
        ]);

        if($validator->fails()){
            $response = [
                'code' => 102, 
                'info' => 'You must fill input field.', 
                'data'  =>  $validator->errors()
            ];

            return response()->json($response, 401);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $token = $user->createToken('bearer')->accessToken;

            $response = [
                'code' => 0, 
                'info' => 'Login success.', 
                'data'  =>  [
                    'accessToken' => $token,
                ]
            ];

            return response()->json($response, 200);
        }
        
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    public function refresh() {
        $user = Auth::user();
        $token = Auth::refresh();

        return response()->json([
            'code' => 0, 
            'info' => 'Token has been refreshed.', 
            'data'  =>  [
                'accessToken' => $token,
            ]
        ]);
    }
}
