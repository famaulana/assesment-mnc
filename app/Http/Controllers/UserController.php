<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;

class UserController extends Controller
{
    public function register(Request $request){
        $validator = Validator::make($request->all(), [
           'nik' => 'required',
           'name' => 'required',
           'email' => 'email|required',
           'password' => 'required',
           'alamat' => 'required',
           'status_perkawinan' => 'required',
        ]);

        if($validator->fails()){
            $response = [
                'code' => 102, 
                'info' => 'You must fill input field.', 
                'data'  =>  $validator->errors()
            ];

            return response()->json($response, 401);
        }

        if(User::where('nik', $request->input('nik', TRUE))->first()){
            return response()->json(['error' => 'User with NIK '.$request->input('nik', TRUE).' already registered.'], 401);
        }

        $response = [
            'code' => 0, 
            'info' => 'Data NIK '.$request->input('nik', TRUE).' has been created.', 
            'data'  =>  [
                'nik' => $request->input('nik', TRUE),
                'name' => $request->input('name', TRUE),
                'email' => $request->input('email', TRUE),
                'password' => $request->input('password', TRUE),
                'alamat' => $request->input('alamat', TRUE),
                'status_perkawinan' => $request->input('status_perkawinan', TRUE),
            ]
        ];

        return response()->json($response, 200);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
           'nik' => 'required',
           'name' => 'required',
           'email' => 'email|required',
           'old_password' => 'required',
           'password' => 'required',
           'alamat' => 'required',
           'status_perkawinan' => 'required',
        ]);

        if($validator->fails()){
            $response = [
                'code' => 102, 
                'info' => 'You must fill input field.', 
                'data'  =>  $validator->errors()
            ];

            return response()->json($response, 401);
        }

        $auth = Auth::user();
        $roles = $this->checkRoles(Role::where('id', $auth->id)->first());

        if(!$roles){
            if($auth->nik !== $request->input('nik', TRUE)){
                $response = [
                    'code' => 300, 
                    'info' => 'Access Denied.', 
                    'data'  =>  null
                ];
        
                return $response;
            }
        }

        $userData = User::where('nik', $request->input('nik', TRUE))->first();

        if(!$userData){
            $response = [
                'code' => 99,
                'info' => "Can't find user with NIK ".$request->input('nik', TRUE).".", 
                'data' =>  $request->all()
            ];

            return response()->json($response, 401);
        }else{
            if(!Hash::check($request->input('old_password', TRUE), $userData->password)){
                $response = [
                    'code' => 10,
                    'info' => 'You inputed wrong old password.', 
                    'data' =>  $request->all()
                ];
                return response()->json($response, 401);
            }
        }

        User::where('nik', $request->input('nik', TRUE))->update([
            'name' => $request->input('name', TRUE),
            'email' => $request->input('email', TRUE),
            'password' => Hash::make($request->input('password', TRUE)),
            'alamat' => $request->input('alamat', TRUE),
            'status_perkawinan' => $request->input('status_perkawinan', TRUE),
        ]);

        $response = [
            'code' => 0, 
            'info' => 'Data NIK '.$request->input('nik', TRUE).' updated.', 
            'data'  =>  User::where('nik', $request->input('nik', TRUE))->first()
        ];

        return response()->json($response, 200);
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
           'nik' => 'required',
        ]);

        if($validator->fails()){
            $response = [
                'code' => 102, 
                'info' => 'You must fill input field.', 
                'data'  =>  $validator->errors()
            ];
            return response()->json($response, 401);
        }

        if(!User::where('nik', $request->input('nik', TRUE))->first()){
            $response = [
                'code' => 99,
                'info' => "Can't find user with NIK ".$request->input('nik', TRUE).".", 
                'data' =>  $request->all()
            ];

            return response()->json($response, 401);
        }

        User::where('nik', $request->input('nik', TRUE))->delete();

        $response = [
            'code' => 0, 
            'info' => 'Data NIK '.$request->input('nik', TRUE).' has been deleted.', 
            'data'  =>  null
        ];

        return response()->json($response, 200);
    }

    public function getUser() {
        return User::paginate(2);
    }

    public function checkRoles($roles){
        if(!$roles){
            return false;
        }else if($roles->role == 'admin'){
            return true;
        }
            
        return false;        
    }
}
