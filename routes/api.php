<?php

use Illuminate\Support\Facades\Route;

//Auth 
Route::post('/login', [App\Http\Controllers\AuthController::class, 'login']);
Route::get('/refreshToken', [App\Http\Controllers\AuthController::class, 'refresh']);

//User
Route::post('/user/register', [App\Http\Controllers\UserController::class, 'register']);

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/user/update', [App\Http\Controllers\UserController::class, 'update']);
    Route::get('/getUser', [App\Http\Controllers\UserController::class, 'getUser']);
    Route::post('/user/delete', [App\Http\Controllers\UserController::class, 'delete']);
});